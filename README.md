# README #

EOS - Directory of Equation of State (EOS) Files


### What is this repository for? ###

This is a library of EOS files compatible with the RNS code.

EOSINDEX.txt includes information about the different EOS files.

### File Format ###

The format required is as follows:
line 1) number of tabulated points in file
remaining lines) four columns:

column 1) energy density/c^2 	(g/cm^3)
column 2) pressure		(dynes/cm^2)
column 3) enthalpy 		(cm^2/s^2)
column 4) baryon number density (cm^{-3})

### Who do I talk to? ###

* Sharon morsink@ualberta.ca